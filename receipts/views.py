from math import log
from django.shortcuts import render,redirect
from receipts.models import ExpenseCategory, Receipt, Account
from receipts.forms import ReceiptForm, CategoryForm, AccountForm
from django.contrib.auth.decorators import login_required



# Create your views here.

@login_required
def receipt_list(request):
    context = {
        'receipts' : Receipt.objects.filter(purchaser=request.user)
    }
    return render(request,'receipts/receipts_list.html',context)



@login_required
def new_receipt(request):

    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect('home')
    else:
        form = ReceiptForm()
    context = {
        'form' : form,
    }
    return render(request,'receipts/new_receipt.html',context)


@login_required
def create_category(request):

    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect('category_list')
    else:
        form = CategoryForm()
    context = {
        'form' : form,
    }
    return render(request,'receipts/new_category.html',context)




@login_required
def create_account(request):

    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect('account_list')
    else:
        form = AccountForm()
    context = {
        'form' : form,
    }
    return render(request,'receipts/new_account.html',context)




@login_required
def list_expense_categories(request):
    
    categories = ExpenseCategory.objects.filter(owner = request.user)

    context = {
        'categories' : categories,
    }

    return render(request,'receipts/categories.html',context)




def list_accounts(request):

    accounts = Account.objects.filter(owner = request.user)

    context = {
        'accounts': accounts,
    }
    return render(request,'receipts/accounts.html',context)
