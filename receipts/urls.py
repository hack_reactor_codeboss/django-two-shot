from receipts.views import create_account, create_category, list_accounts, list_expense_categories, new_receipt, receipt_list
from django.urls import path



urlpatterns = [
    path('',receipt_list,name='home'),
    path('create/',new_receipt,name="create_receipt"),
    path('categories/',list_expense_categories,name='category_list'),
    path('accounts/',list_accounts,name="account_list"),
    path('categories/create/', create_category,name="create_category"),
    path('accounts/create/',create_account,name="create_account"),
]
