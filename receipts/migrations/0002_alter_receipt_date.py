# Generated by Django 5.0 on 2023-12-14 17:23

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("receipts", "0001_initial"),
    ]

    operations = [
        migrations.AlterField(
            model_name="receipt",
            name="date",
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
