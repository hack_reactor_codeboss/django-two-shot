from django.urls import path
from accounts.views import sign_up_user, user_login,user_logout



urlpatterns = [
    path('login/',user_login,name="login"),
    path('logout/', user_logout,name='logout'),
    path('signup/',sign_up_user,name='signup'),
]
